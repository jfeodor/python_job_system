import logging
import os
from pathlib import Path
import sys

from numerous.client import NumerousClient, ScenarioStatus

import python_job

logging.basicConfig(level=logging.INFO)


if __name__ == '__main__':
    try:
        impl = python_job.get_hydrated_system().implementation
        [entrypoint] = impl.components.find('Entrypoint')
        entrypoint_func = python_job.get_entrypoint_module_from_base(Path('/app', impl.context), entrypoint)
    except:
        logging.exception('Failed initializing entrypoint')
        sys.exit(1)

    with NumerousClient(
        secure=os.getenv('FORCE_INSECURE', 'False') != 'True',
        server=os.environ['NUMEROUS_API_HOST'],
        port=os.environ['NUMEROUS_API_PORT'],
        refresh_token=os.environ['NUMEROUS_REFRESH_TOKEN'],
        execution_id=os.environ['NUMEROUS_LAUNCH_ID'],
        project=os.environ['NUMEROUS_PROJECT_ID'],
        scenario=os.environ['NUMEROUS_SCENARIO_ID'],
        job_id=os.environ['NUMEROUS_JOB_ID']
    ) as client:
        try:
            client.set_scenario_progress('Bootstrapping', ScenarioStatus.RUNNING, force=True)
            entrypoint_func(client, impl)
        except:
            logging.exception('Job failed')
            sys.exit(1)
        else:
            sys.exit(0)
