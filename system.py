from abc import ABC
from dataclasses import dataclass, field
from pathlib import PurePosixPath
import typing


ComponentValue = typing.Union[str, float, PurePosixPath]


class ResolveError(Exception):
    pass


@dataclass
class Connector:
    parent: typing.Union['HydratedSystem', 'Component']
    kind: str
    name: str
    type: str
    source: typing.Optional[PurePosixPath]

    def _resolve(self, path: PurePosixPath, current: 'SpecItem') -> 'Component':
        if not path.parts:
            if isinstance(current, HydratedSystem):
                raise ResolveError()
            elif current.value is None:
                raise ResolveError()
            else:
                return current

        path_head = path.parts[0]
        path_tail = PurePosixPath(*path.parts[1:])
        if path_head == '..':
            if current.parent is None:
                raise ResolveError()
            else:
                return self._resolve(path_tail, current.parent)
        elif isinstance(current, Component):
            if path_head in ('outputs', 'inputs'):
                return current
            else:
                raise ResolveError()
        elif (components := current.components.find(name=path.parts[0])):
            return self._resolve(path_tail, components[0])
        elif (systems := [sys for sys in current.spec if isinstance(sys, HydratedSystem) and sys.name == path.parts[0]]):
            return self._resolve(path_tail, systems[0])
        else:
            raise ResolveError()


    def resolve(self) -> 'Component':
        if self.source is None:
            raise ResolveError()

        return self._resolve(self.source, self.parent)


@dataclass
class Output(Connector):

    @staticmethod
    def from_dict(parent: typing.Union['HydratedSystem', 'Component'], data: dict[str, typing.Any]) -> 'Output':
        return Output(
            parent,
            'output',
            data['name'],
            data['type'],
            None if data.get('source') is None else PurePosixPath(data['source']),
        )


@dataclass
class Input(Connector):

    @staticmethod
    def from_dict(parent: typing.Union['HydratedSystem', 'Component'], data: dict[str, typing.Any]) -> 'Input':
        return Input(
            parent,
            'input',
            data['name'],
            data['type'],
            None if data.get('source') is None else PurePosixPath(data['source']),
        )


class SpecItem(ABC):
    name: str
    parent: 'SpecItem'
    inputs: list[Input]
    outputs: list[Output]

    def input(self, name: str) -> Input:
        for input in self.inputs:
            if input.name == name:
                return input
        raise RuntimeError()

    def output(self, name: str) -> Output:
        for output in self.outputs:
            if output.name == name:
                return output
        raise RuntimeError()


@dataclass
class Component(SpecItem):
    parent: 'HydratedSystem'
    type: str
    name: str
    value: typing.Optional[ComponentValue]
    inputs: list[Input] = field(default_factory=list)
    outputs: list[Output] = field(default_factory=list)

    @staticmethod
    def from_dict(parent: 'HydratedSystem', data: dict[str, typing.Any]) -> 'Component':
        component = Component(
            parent,
            data['type'],
            data['name'],
            data.get('value'),
        )
        inputs = [Input.from_dict(component, i) for i in data.get('inputs', [])]
        outputs = [Output.from_dict(component, o) for o in data.get('outputs', [])]
        component.inputs = inputs
        component.outputs = outputs
        return component


class ComponentSet():

    def __init__(self, components: list[Component]):
        self._components = components

    def find(self, type: typing.Optional[str] = None, name: typing.Optional[str] = None) -> list[Component]:
        if type is None and name is None:
            return []
        comps = []
        for comp in self._components:
            if type is not None and type != comp.type:
                continue
            if name is not None and name != comp.name:
                continue
            comps.append(comp)
        return comps


@dataclass
class HydratedSystem(SpecItem):
    implementation: typing.Optional['HydratedSystem']
    context: typing.Optional[str]
    parent: typing.Optional['HydratedSystem']
    name: str
    spec: list[SpecItem] = field(default_factory=list)
    inputs: list[Input] = field(default_factory=list)
    outputs: list[Output] = field(default_factory=list)

    @property
    def components(self) -> ComponentSet:
        return ComponentSet([c for c in self.spec if isinstance(c, Component)])

    @staticmethod
    def from_dict(data: dict[str, typing.Any], parent: typing.Optional['HydratedSystem'] = None) -> 'HydratedSystem':
        hydrated_system = HydratedSystem(
            implementation=None if data.get('implementation') is None else HydratedSystem.from_dict(data['implementation']),
            context=data.get('context'),
            parent=parent,
            name=data['name']
        )
        spec = [
            HydratedSystem.from_dict(item, hydrated_system) if 'system' in item
            else Component.from_dict(hydrated_system, item)
            for item in data['spec']
        ]
        hydrated_system.spec = spec
        inputs = [Input.from_dict(hydrated_system, i) for i in data.get('inputs', [])]
        outputs = [Output.from_dict(hydrated_system, o) for o in data.get('outputs', [])]
        hydrated_system.inputs = inputs
        hydrated_system.outputs = outputs

        return hydrated_system
