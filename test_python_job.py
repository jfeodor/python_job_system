import json

import python_job


def test_deserialize_hydrated_system(monkeypatch):
    monkeypatch.setenv(
        "HYDRATED_SYSTEM",
        json.dumps(
            {
                "name": "Python Job System",
                "type": "Job",
                "spec": [
                    {
                        "name": "Python Job Dockerfile",
                        "type": "Dockerfile",
                        "inputs": [
                            {
                                "name": "Path",
                                "type": "Path",
                                "source": "../Python Dockerfile/outputs/Path",
                            }
                        ],
                        "outputs": [],
                        "value": None,
                    },
                    {
                        "name": "Python Dockerfile",
                        "type": "PathValue",
                        "inputs": [],
                        "outputs": [{"name": "Path", "type": "Path", "source": None}],
                        "value": "Dockerfile",
                    },
                ],
                "context": "gitlab.com/jfeodor/python_job_system/80296fe5cbeeaa5fb1341299ce50806ae5bf1469",
                "inputs": [],
                "outputs": [],
                "interface": [
                    {
                        "type": "Entrypoint",
                        "inputs": [
                            {"name": "SourceRoot", "type": "Path", "optional": False},
                            {
                                "name": "EntrypointModule",
                                "type": "String",
                                "optional": False,
                            },
                            {"name": "Requirements", "type": "Path", "optional": False},
                        ],
                        "outputs": [],
                        "exactly": 1,
                    }
                ],
                "implementation": {
                    "name": "Arithmetics System",
                    "type": {
                        "repository": {
                            "provider": "gitlab.com",
                            "group": "jfeodor",
                            "name": "python_job_system",
                        },
                        "version": "80296fe5cbeeaa5fb1341299ce50806ae5bf1469",
                    },
                    "spec": [
                        {
                            "name": "Arithmetics Entrypoint",
                            "type": "Entrypoint",
                            "inputs": [
                                {
                                    "name": "SourceRoot",
                                    "type": "Path",
                                    "source": "../ArithmeticsSourceRoot/outputs/Path",
                                },
                                {
                                    "name": "EntrypointModule",
                                    "type": "String",
                                    "source": "../ArithmeticsModule/outputs/String",
                                },
                                {
                                    "name": "Requirements",
                                    "type": "Path",
                                    "source": "../ArithmeticsRequirements/outputs/Path",
                                },
                            ],
                            "outputs": [],
                            "value": None,
                        },
                        {
                            "name": "ArithmeticsSourceRoot",
                            "type": "PathValue",
                            "inputs": [],
                            "outputs": [
                                {"name": "Path", "type": "Path", "source": None}
                            ],
                            "value": "src",
                        },
                        {
                            "name": "ArithmeticsModule",
                            "type": "StringValue",
                            "inputs": [],
                            "outputs": [
                                {"name": "String", "type": "String", "source": None}
                            ],
                            "value": "arithmetics:run",
                        },
                        {
                            "name": "ArithmeticsRequirements",
                            "type": "PathValue",
                            "inputs": [],
                            "outputs": [
                                {"name": "Path", "type": "Path", "source": None}
                            ],
                            "value": "requirements.txt",
                        },
                    ],
                    "context": "gitlab.com/jfeodor/arithmetics_system/c2f7f23abee9aa6f0b8cc38da73800bdaa698a65",
                    "inputs": [],
                    "outputs": [],
                    "interface": [
                        {
                            "type": "Plus",
                            "inputs": [
                                {"name": "X", "type": "Float", "optional": False},
                                {"name": "Y", "type": "Float", "optional": False},
                            ],
                            "outputs": [
                                {"name": "Result", "type": "Float", "optional": False}
                            ],
                            "exactly": None,
                        },
                        {
                            "type": "Result",
                            "inputs": [
                                {"name": "Result", "type": "Float", "optional": False}
                            ],
                            "outputs": [],
                            "exactly": 1,
                        },
                    ],
                    "implementation": {
                        "name": "My Addition System",
                        "type": {
                            "repository": {
                                "provider": "gitlab.com",
                                "group": "jfeodor",
                                "name": "arithmetics_system",
                            },
                            "version": "c2f7f23abee9aa6f0b8cc38da73800bdaa698a65",
                        },
                        "spec": [
                            {
                                "name": "My Plus",
                                "type": "Plus",
                                "inputs": [
                                    {
                                        "name": "x",
                                        "type": "Float",
                                        "source": "../X/outputs/Float",
                                    },
                                    {
                                        "name": "y",
                                        "type": "Float",
                                        "source": "../Y/outputs/Float",
                                    },
                                ],
                                "outputs": [
                                    {"name": "Result", "type": "Float", "source": None}
                                ],
                                "value": None,
                            },
                            {
                                "name": "X",
                                "type": "FloatValue",
                                "inputs": [],
                                "outputs": [
                                    {"name": "Float", "type": "Float", "source": None}
                                ],
                                "value": 10.0,
                            },
                            {
                                "name": "Y",
                                "type": "FloatValue",
                                "inputs": [],
                                "outputs": [
                                    {"name": "Float", "type": "Float", "source": None}
                                ],
                                "value": 10.0,
                            },
                        ],
                        "context": "gitlab.com/jfeodor/my_addition_system/4df329e572d3d9ab8f6f71caf3a3ece5ac4a5d49",
                        "inputs": [],
                        "outputs": [
                            {
                                "name": "Result",
                                "type": "Float",
                                "source": "My Plus/outputs/Result",
                            }
                        ],
                        "interface": None,
                        "implementation": None,
                    },
                },
            }
        ),
    )

    hydrated_system = python_job.get_hydrated_system()
    assert hydrated_system.name == "Python Job System"
    assert hydrated_system.implementation.name == "Arithmetics System"
    components = hydrated_system.implementation.components.find(type="Entrypoint")
    assert len(components) == 1
    entrypoint = components[0]
    assert entrypoint.name == "Arithmetics Entrypoint"
    assert entrypoint.input("SourceRoot").name == "SourceRoot"
    assert entrypoint.input("SourceRoot").resolve().value == "src"
    my_addition_system = hydrated_system.implementation.implementation
    assert my_addition_system.output("Result").resolve().name == "My Plus"