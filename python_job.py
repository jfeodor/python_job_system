import importlib.util
import json
import os
from pathlib import Path
import sys
from typing import Any, Callable

from system import HydratedSystem, Component, ResolveError


def get_hydrated_system() -> HydratedSystem:
    hydrated_system_data = json.loads(os.environ['HYDRATED_SYSTEM'])
    return HydratedSystem.from_dict(hydrated_system_data)


def extract_entrypoint(impl: HydratedSystem) -> Component:
    [entrypoint] = impl.components.find(type='Entrypoint')
    return entrypoint


def resolve_source_root(entrypoint: Component) -> str:
    try:
        entrypoint_source_root = entrypoint.input('SourceRoot').resolve().value
    except ResolveError:
        print('Could not resolve SourceRoot')
        sys.exit(1)

    try:
        entrypoint_module: str = entrypoint.input('EntrypointModule').resolve().value  # type: ignore
        _, _ = entrypoint_module.split(':')
    except:
        print('Invalid EntrypointModule')
        sys.exit(1)

    if not isinstance(entrypoint_source_root, str):
        print(f'Invalid SourceRoot {repr(entrypoint_source_root)}')
        sys.exit(1)

    return entrypoint_source_root


def get_entrypoint_module_from_base(context: Path, entrypoint: Component) -> Callable[..., Any]:
    source_root = resolve_source_root(entrypoint)

    entrypoint_module_value = entrypoint.input('EntrypointModule').resolve().value
    if not isinstance(entrypoint_module_value, str):
        raise ValueError(f'Entrypoint module must be a String: {repr(entrypoint_module_value)}')

    module_name, entrypoint_value = entrypoint_module_value.split(':')
    spec = importlib.util.spec_from_file_location(module_name, context / source_root)
    if spec is None:
        spec = importlib.util.spec_from_file_location(module_name, context / source_root / f'{module_name}.py')
    if spec is None:
        raise ValueError(f'Could not find module {module_name} in {repr(str(context / source_root))}')

    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    if spec.loader is None:
        raise RuntimeError('ModuleSpec has no loader')

    spec.loader.exec_module(module)
    return getattr(module, entrypoint_value)  # type: ignore
