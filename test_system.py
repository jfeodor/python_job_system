from system import HydratedSystem

def test_hydrated_system():
    system = HydratedSystem.from_dict({
        'system': object(),
        'name': 'system',
        'context': '/my/context/path',
        'spec': [{
            'component': 'Component',
            'name': 'My Component',
            'inputs': [{
                'name': 'My Input',
                'source': '../My Other Component/outputs/String',
                'type': 'String'
            }]
        }, {
            'component': 'StringValue',
            'name': 'My Other Component',
            'value': 'My String Value',
            'inputs': [{
                'name': 'My Input',
                'type': 'String'
            }]
        }]
    })

    components = system.components.find(type='Component')
    assert len(components) == 1
    [component] = components
    assert component.name == 'My Component'
    assert component.input('My Input').resolve().value == 'My String Value'
