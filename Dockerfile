FROM python:3.9
ARG BOOTSTRAP_CONTEXT_DIRECTORY
ARG HYDRATED_SYSTEM

RUN pip install --extra-index-url=https://pypi.numerously.com/simple numerous_api_client==0.16.5

ENV BOOTSTRAP_CONTEXT_DIRECTORY=${BOOTSTRAP_CONTEXT_DIRECTORY}
ENV HYDRATED_SYSTEM=${HYDRATED_SYSTEM}
COPY . /app
WORKDIR /app/${BOOTSTRAP_CONTEXT_DIRECTORY}
RUN python build.py

WORKDIR /app
COPY ${BOOTSTRAP_CONTEXT_DIRECTORY}/docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT [ "bash", "/docker-entrypoint.sh" ]
