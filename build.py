from pathlib import Path
import sys
import shutil
import subprocess

import python_job
from system import HydratedSystem, Component, ResolveError


def copy(source: str, destination: str) -> None:
    shutil.copytree(source, destination, dirs_exist_ok=True)


def pip_install_requirements(requirements: str) -> int:
    return subprocess.call(['pip', 'install', '-r', requirements])


def build_implementation(impl: HydratedSystem) -> None:
    components = impl.components.find('BuildEntrypoint')
    if not components:
        return

    if len(components) > 1:
        print('Can have at most 1 BuildEntrypoint components')
        sys.exit(1)

    build_entrypoint_comp = components[0]

    if impl.context is None:
        print('Implementation must have a context')
        sys.exit(1)

    build_entrypoint = python_job.get_entrypoint_module_from_base(Path('/app', impl.context), build_entrypoint_comp)
    build_entrypoint(impl)


def validate_source_root(context: Path, entrypoint: Component) -> Path:
    entrypoint_source_root = python_job.resolve_source_root(entrypoint)

    if not (context / entrypoint_source_root).exists():
        print(f'Could not find source root {context / entrypoint_source_root}')
        sys.exit(1)

    return context / entrypoint_source_root


def install_entrypoint_requirements(context: Path, entrypoint: Component) -> None:
    try:
        requirements_path = entrypoint.input('Requirements').resolve().value
    except ResolveError:
        return

    if not isinstance(requirements_path, str):
        print(f'Invalid requirements path: {requirements_path}')
        sys.exit(1)

    if not (context / requirements_path).exists():
        print(f'Invalid requirements path: {requirements_path}, not found in context {context}')
        sys.exit(1)

    pip_install_requirements(str(context / requirements_path))


def validate_implementation(hydrated_system: HydratedSystem) -> HydratedSystem:
    if hydrated_system.implementation:
        return hydrated_system.implementation
    else:
        print('Missing implementation')
        sys.exit(1)


if __name__ == '__main__':
    hydrated_system = python_job.get_hydrated_system()
    impl = validate_implementation(hydrated_system)
    entrypoint = python_job.extract_entrypoint(impl)
    context = Path('/app', impl.context)
    install_entrypoint_requirements(context, entrypoint)
    validate_source_root(context, entrypoint)
    build_implementation(impl)

    print('Build completed')
    sys.exit(0)
